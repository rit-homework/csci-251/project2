﻿using System;
using System.IO;
using CommandLine;

namespace Project2
{
    internal enum TestMethods
    {
        seq,
        par,
        both
    }

    [Verb("testwith")]
    internal class TestOptions
    {
        [Option('t')] public TestMethods Type { get; set; }

        [Value(0)] public int TestCount { get; set; }
    }

    internal class Options
    {
        [Option('g', "", Default = false)] public bool ParseInt { get; set; }

        [Value(0, Required = true, MetaName = "filename",
            HelpText = "The name of the file whose contents you want to sort")]
        public string FileName { get; set; }
    }

    internal class ArgumentParser
    {
        static readonly string HELP = @"Project2 [<command>|filename]

Commands are listed below:

testwith -t <type> <count>    Runs a test scenario for quicksort
                              type can be:
                                 seq - sequential quicksort
                                 par - parallel quicksort
                                 both - both sequential and parallel

when no command is given, you can specify a filename, and the
optional -g.  -g will force the data to be integer types

Example:
Project2 -g test.txt
Project2 testwith -t par 10000
Project2 test.txt
";

        internal static void parse(string[] args, Action<TestOptions> runTest, Action<Options> runDefault)
        {
            var parser = new Parser(with => with.HelpWriter = TextWriter.Null);
            parser.ParseArguments<TestOptions, Options>(args)
                .WithParsed(runTest)
                .WithNotParsed(outerErr =>
                    parser.ParseArguments<Options>(args).WithParsed(runDefault)
                        .WithNotParsed(err => Console.WriteLine(ArgumentParser.HELP))
                );
        }
    }
}