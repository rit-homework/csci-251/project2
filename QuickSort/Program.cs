﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Project2
{
    class Project2
    {
        public static void Main(string[] args)
        {
            ArgumentParser.parse(args, options =>
            {
                var numbers = Utils.GenerateNumbers(options.TestCount);

                Console.WriteLine("Detected {0} Processors", Environment.ProcessorCount);

                if (options.Type == TestMethods.seq || options.Type == TestMethods.both)
                {
                    var cloned = numbers.Copy();
                    Console.WriteLine("Sequential");
                    Console.WriteLine(Utils.MeasureRuntime(() => cloned.QuicksortSequential()));
                }

                if (options.Type == TestMethods.par || options.Type == TestMethods.both)
                {
                    var cloned = numbers.Copy();
                    Console.WriteLine("Parallel");
                    Console.WriteLine(Utils.MeasureRuntime(() => cloned.QuicksortParallel()));
                }
            }, options =>
            {
                var lines = File.ReadAllLines(options.FileName);
                if (options.ParseInt)
                {
                    var converted = lines.Select(int.Parse).ToArray();
                    Sort(converted);
                }
                else
                {
                    Sort(lines);
                }
            });
        }

        private static void Sort<T>(IList<T> lines)
            where T : IComparable
        {
            lines.QuicksortParallel();

            foreach (var line in lines)
            {
                Console.WriteLine(line);
            }
        }
    }
}