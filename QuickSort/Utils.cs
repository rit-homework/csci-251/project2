using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Project2
{
    public static class Utils
    {
        public static void Swap<T>(this IList<T> list, int startIdx, int endIdx)
        {
            T temp = list[startIdx];
            list[startIdx] = list[endIdx];
            list[endIdx] = temp;
        }

        public static List<int> GenerateNumbers(int size)
        {
            Random random = new Random(10);
            return Enumerable.Range(0, size).Select(_ => random.Next()).ToList();
        }

        public static TimeSpan MeasureRuntime(Action inner)
        {
            var stopwatch = Stopwatch.StartNew();
            inner();
            stopwatch.Stop();

            return stopwatch.Elapsed;
        }

        public static List<T> Copy<T>(this IList<T> list)
        {
            return list.ToList();
        }
    }
}