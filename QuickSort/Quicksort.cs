using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project2
{
    public static class Quicksort
    {
        public static void QuicksortParallel<T>(this IList<T> list)
            where T : IComparable
        {
            list.QuicksortParallelHelper(0, list.Count - 1).Wait();
        }

        public static void QuicksortSequential<T>(this IList<T> list)
            where T : IComparable
        {
            list.QuicksortSequentialHelper(0, list.Count - 1);
        }

        static async Task QuicksortParallelHelper<T>(this IList<T> list, int leftIdx, int rightIdx)
            where T : IComparable
        {
            if (leftIdx >= rightIdx)
            {
                return;
            }

            int length = rightIdx - leftIdx + 1;
            if (length < 10_000)
            {
                await Task.Run(() => list.QuicksortSequentialHelper(leftIdx, rightIdx));
            }
            else
            {
                int middleIdx = list.Partition(leftIdx, rightIdx);
                var left = list.QuicksortParallelHelper(leftIdx, middleIdx - 1);
                var right = list.QuicksortParallelHelper(middleIdx, rightIdx);

                await Task.WhenAll(left, right);
            }
        }

        static void QuicksortSequentialHelper<T>(this IList<T> list, int leftIdx, int rightIdx)
            where T : IComparable
        {
            if (leftIdx >= rightIdx)
            {
                return;
            }

            int middleIdx = list.Partition(leftIdx, rightIdx);
            list.QuicksortSequentialHelper(leftIdx, middleIdx - 1);
            list.QuicksortSequentialHelper(middleIdx, rightIdx);
        }

        static int Partition<T>(this IList<T> list, int leftIdx, int rightIdx)
            where T : IComparable
        {
            int pivotIdx = (leftIdx + rightIdx) / 2;
            T pivot = list[pivotIdx];

            while (leftIdx <= rightIdx)
            {
                while (list[leftIdx].CompareTo(pivot) < 0)
                {
                    leftIdx++;
                }

                while (list[rightIdx].CompareTo(pivot) > 0)
                {
                    rightIdx--;
                }

                if (leftIdx > rightIdx)
                {
                    break;
                }

                list.Swap(leftIdx, rightIdx);
                leftIdx++;
                rightIdx--;
            }

            return leftIdx;
        }
    }
}