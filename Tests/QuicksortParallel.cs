using System;
using System.Collections.Generic;
using Project2;
using Xunit;

namespace Tests
{
    public class QuicksortParallel
    {
        [Fact]
        public void SortsManyNumbers()
        {
            IList<int> numbers = Utils.GenerateNumbers(1_000_000);
            numbers.QuicksortParallel();
            Console.WriteLine(numbers.ToString());

            var sorted = numbers.Copy();
            sorted.Sort();

            Assert.Equal(sorted, numbers);
        }
    }
}