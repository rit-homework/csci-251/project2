using System;
using System.Collections.Generic;
using Project2;
using Xunit;

namespace Tests
{
    public class QuicksortSequential
    {
        [Fact]
        public void SortsNumbers()
        {
            IList<int> numbers = new List<int>() {9, 2, 6, 4, 3, 5, 1};
            numbers.QuicksortSequential();
            Console.WriteLine(numbers.ToString());

            var sorted = numbers.Copy();
            sorted.Sort();

            Assert.Equal(sorted, numbers);
        }
        
        [Fact]
        public void SortsManyNumbers()
        {
            IList<int> numbers = Utils.GenerateNumbers(1_000_000);
            numbers.QuicksortParallel();
            Console.WriteLine(numbers.ToString());

            var sorted = numbers.Copy();
            sorted.Sort();

            Assert.Equal(sorted, numbers);
        }
        
        [Fact]
        public void SortsNumbersWithDuplicates()
        {
            IList<int> numbers = new List<int>() {9, 2, 6, 4, 3, 5, 1, 3, 3, 3};
            numbers.QuicksortSequential();
            Console.WriteLine(numbers.ToString());

            var sorted = numbers.Copy();
            sorted.Sort();

            Assert.Equal(sorted, numbers);
        }
    }
}