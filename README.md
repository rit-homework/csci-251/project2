# quicksort

This program implements two versions of QuickSort: a parallel one and a
sequential one.

The parallel version is consistently a bit faster. Here's some sample output.

```
$ dotnet run -t both 10000000
Detected 4 Processors
Sequential
00:00:08.6079887
Parallel
00:00:06.8533330
```

QuicksortParallel partitions the input list into two partitions, sorting each of
the two partitions individually. If there are less than 10k items being
considered at each level of recursion, instead of continuing parallelly, the
quicksort continues sequentially. A task which resolves when both sides are
sorted is returned.
